import {Component, Inject, OnInit} from '@angular/core';
import {Router, ActivatedRoute} from '@angular/router';
import {CrudService} from '../services/crud.service';
import {MatSnackBar, MatDialog, MAT_DIALOG_DATA} from '@angular/material';
import {faAmazon} from '@fortawesome/free-brands-svg-icons';
import {User, Config} from '../services/models';
import {Title} from '@angular/platform-browser';

export interface DialogData {
    username?: string;
    fulltag?: string;
    passbackTag?: string;
}

@Component({
    selector: 'app-data-table',
    templateUrl: './data-table.component.html',
    styleUrls: ['./data-table.component.scss']
})
export class DataTableComponent implements OnInit {

    constructor(
        private router: Router,
        private crudService: CrudService,
        public dialog: MatDialog,
        private snackBar: MatSnackBar,
        private activatedRoute: ActivatedRoute,
        private titleService: Title
    ) {
    }

    public currentRoute: string;
    public currentId = '';
    public users: User[] = [];
    public tempUsers: User[] = [];
    public configs: Config[] = [];
    public tempConfigs: Config[] = [];
    public usersReady = false;
    public configsReady = false;
    public title = '';
    public linkTitle = '';
    public currentUsername = '';
    public faAmazon = faAmazon;
    loading = true;

    async ngOnInit() {
        this.titleService.setTitle('WBidder | ' + this.activatedRoute.snapshot.data.title);
        this.activatedRoute.queryParams.subscribe(data => {
            if (data.id) {
                this.currentId = data.id;
            }
        });
        this.currentRoute = this.getCurrentRoute();
        if (this.currentRoute.includes('users')) {
            this.title = 'All users';
            this.linkTitle = 'New User';
            await this.getAllUsers();
        } else {
            const id = this.currentId;
            if (!id) {
                this.router.navigate(['users'])
                    .catch(e => console.log(e));
            }
            this.linkTitle = 'New Config';
            await this.getUserConfigs(id);
        }
    }

    getCurrentRoute() {
        return this.router.routerState.snapshot.url.replace('/', '');
    }

    getAllUsers() {
        this.crudService.getUsers().subscribe(data => {
            this.users = data;
            this.tempUsers = this.users;
            this.usersReady = true;
            this.loading = false;
        });
    }

    getUserConfigs(id) {
        localStorage.removeItem('currentUser');
        localStorage.removeItem('currentUserId');
        this.crudService.getUserConfigs(id).subscribe(data => {
            this.configs = data.configs;
            this.title = data.name;
            this.tempConfigs = this.configs;
            this.configsReady = true;
            this.loading = false;
            localStorage.setItem('currentUser', this.title);
            localStorage.setItem('currentUserId', id);
        });
    }

    searchUser(event): void {
        const val = event.target.value.toLowerCase();
        this.users = this.tempUsers.filter((d) => {
            return d.name.toLowerCase().indexOf(val) !== -1 || !val;
        });
    }

    searchConfig(event): void {
        const val = event.target.value.toLowerCase();
        this.configs = this.tempConfigs.filter((d) => {
            return d.name.toLowerCase().indexOf(val) !== -1 || !val;
        });
    }

    async deleteUsername(row) {
        if (confirm('Do you really want to delete this publisher and all configs? This action can\'t be undone')) {
            for (let i = 0; i < this.users.length; i++) {
                if (this.users[i].id === row.id) {
                    this.users.splice(i, 1);
                    this.users = [...this.users]; // update main users' object and rerender datatable
                    const res: any = JSON.parse(await this.crudService.deleteUsername(row.id));
                    this.openSnackBar(`Publisher ${res.name} with ID ${res.id} was successfully deleted`, 'OK');
                }
            }
        }
    }

    editUsername(row: { name: string, id: number }) {
        this.currentUsername = row.name;
        const dialogRef = this.dialog.open(DataTableDialogComponent,
            {data: {username: this.currentUsername}});
        dialogRef.afterClosed().subscribe(async (result) => {
            if (!result) {
                return;
            } else {
                const res: any = await this.crudService.editUsername(row.id, result);
                this.users.forEach(user => {
                    if (user.name === this.currentUsername) {
                        user.name = result;
                    }
                });
                this.openSnackBar(res, 'OK');
            }
        });
    }

    async deleteConfig(row: { id: number }) {
        if (confirm('Do you really want to delete this config? This action can\'t be undone!')) {
            for (let i = 0; i < this.configs.length; i++) {
                if (this.configs[i].id === row.id) {
                    this.configs.splice(i, 1);
                    this.configs = [...this.configs]; // update main configs' object and rerender datatable
                    const res: any = await this.crudService.deleteConfig(row.id);
                    this.openSnackBar(res, 'OK');
                }
            }
        }
    }

    async showCode(row: { id: number }) {
        const res: any = await this.crudService.getTags(row.id);
        this.dialog.open(ShowTagsDialogComponent,
            {data: {fulltag: res.fulltag, passbackTag: res.passbackTag}});
    }

    openSnackBar(message: string, action: string) {
        this.snackBar.open(message, action, {
            duration: 5000,
        });
    }

}


@Component({
    selector: 'app-data-table-dialog-component',
    templateUrl: './data-table-dialog-component.html',
})
export class DataTableDialogComponent {
    constructor(@Inject(MAT_DIALOG_DATA)
                public data: DialogData) {
    }
}

@Component({
    selector: 'app-show-tags-dialog-component',
    templateUrl: './show-tags-dialog-component.html',
    styleUrls: ['./show-tags-dialog.component.scss']
})
export class ShowTagsDialogComponent {
    constructor(@Inject(MAT_DIALOG_DATA)
                public data: DialogData) {
    }

    copyTag(event) {
        const target = event.target;
        target.select();
        document.execCommand('copy');
        window.getSelection().removeAllRanges();
    }
}
