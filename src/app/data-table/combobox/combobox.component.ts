import {COMMA, ENTER} from '@angular/cdk/keycodes';
import {Component, ElementRef, EventEmitter, OnInit, Output, ViewChild, Input, SimpleChanges, OnChanges} from '@angular/core';
import {FormControl, Validators} from '@angular/forms';
import {MatAutocompleteSelectedEvent, MatAutocomplete, MatAutocompleteTrigger} from '@angular/material/autocomplete';
import {MatChipInputEvent} from '@angular/material/chips';
import {Observable} from 'rxjs';
import {map, startWith} from 'rxjs/operators';
import {CrudService} from '../../services/crud.service';

@Component({
    selector: 'app-combobox-component',
    templateUrl: 'combobox.component.html',
    styleUrls: ['combobox.component.scss'],
})

// tslint:disable-next-line:component-class-suffix
export class AdapterSelectCombobox implements OnInit, OnChanges {
    selectable = true;
    removable = true;
    addOnBlur = false;
    separatorKeysCodes: number[] = [ENTER, COMMA];
    adapterCtrl = new FormControl(null, Validators.required);
    filteredAdapters: Observable<string[]>;
    adapters: string[] = [];
    allAdapters: string[] = [];
    emitter: any = new EventEmitter();
    settings: any;
    toAllToggle = false;
    floorPriceValue: any;

    @ViewChild('adapterInput', {static: false}) adapterInput: ElementRef<HTMLInputElement>;
    @ViewChild('auto', {static: false}) matAutocomplete: MatAutocomplete;
    @ViewChild(MatAutocompleteTrigger, {static: false}) inputAutoComplete: MatAutocompleteTrigger;

    @Input() toAll: any;
    @Input() existAdapters: string[];
    @Output() changeAdaptersList = new EventEmitter();

    ngOnChanges(changes: SimpleChanges) {
        if (changes.toAll.currentValue && changes.toAll.currentValue.checked) {
            this.toAllToggle = true;
            this.floorPriceValue = changes.toAll.currentValue.value;
        } else {
            this.toAllToggle = false;
        }
    }

    constructor(
        private crudService: CrudService
    ) {
        this.filteredAdapters = this.adapterCtrl.valueChanges.pipe(
            startWith(null),
            map((adapter: string | null) => adapter ? this._filter(adapter) : this.allAdapters.slice()));
    }

    async ngOnInit() {
        this.crudService.getAllAdapters().subscribe((data: any) => {
            this.allAdapters = data;
            if (this.existAdapters) {
                this.existAdapters.forEach(adapter => {
                    const index = this.allAdapters.indexOf(adapter);
                    if (index > -1) {
                        this.allAdapters.splice(index, 1);
                    }
                });
            }
            this.adapterCtrl.setValue(null);
        });
        this.emitter.subscribe((event) => {
            if (event.type === 'add') {
                const index = this.allAdapters.indexOf(event.adapter);
                if (index !== -1) {
                    this.allAdapters.splice(index, 1);
                }
                this.adapterCtrl.setValue(null);
                this.allAdapters = [...this.allAdapters];
            } else if (event.type === 'remove') {
                this.allAdapters.push(event.adapter);
                this.allAdapters = this.allAdapters.sort();
                this.allAdapters = Array.from(new Set(this.allAdapters));
                this.adapterCtrl.setValue(null);
                this.allAdapters = [...this.allAdapters];
            }
            if (this.adapters.length !== 0) {
                this.getSettings(this.adapters);
            } else {
                this.settings = [];
            }
            this.changeAdaptersList.emit(this.adapters);
        });
    }

    openPanel(event): void {
        event.stopPropagation();
        this.inputAutoComplete.openPanel();
    }

    add(event: MatChipInputEvent): void {
        if (!this.matAutocomplete.isOpen) {
            const input = event.input;
            const value = event.value;

            if ((value || '').trim()) {
                this.adapters.push(value.trim());
            }

            if (input) {
                input.value = '';
            }

            this.emitter.emit({type: 'add', adapter: value.trim()});
            this.adapterCtrl.setValue(null);
        }
    }

    remove(adapter: string): void {
        const index = this.adapters.indexOf(adapter);
        if (index >= 0) {
            this.adapters.splice(index, 1);
        }
        this.adapters = [...this.adapters];
        this.emitter.emit({type: 'remove', adapter});
    }

    selected(event: MatAutocompleteSelectedEvent): void {
        this.adapters.push(event.option.viewValue);
        this.adapterInput.nativeElement.value = '';
        this.adapterCtrl.setValue(null);
        this.emitter.emit({type: 'add', adapter: event.option.viewValue});
    }

    private _filter(value: string): string[] {
        const filterValue = value.toLowerCase();
        return this.allAdapters.filter(adapter => adapter.toLowerCase().indexOf(filterValue) === 0);
    }

    getSettings(adapters) {
        this.crudService.getSettings(adapters).subscribe((data: any) => {
            this.settings = data;
        });
    }
}
