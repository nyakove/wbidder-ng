// external dependencies
import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {FontAwesomeModule} from '@fortawesome/angular-fontawesome';
import {HttpClientModule} from '@angular/common/http';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {NgxDatatableModule} from '@swimlane/ngx-datatable';
import {SocketIoModule, SocketIoConfig} from 'ngx-socket-io';

// internal dependencies
import {AppRoutingModule} from './app-routing.module';
import {WBidderComponent} from './app.component';
import {
    MatAutocompleteModule,
    MatButtonModule,
    MatCardModule,
    MatCheckboxModule,
    MatChipsModule,
    MatDialogModule,
    MatDividerModule,
    MatExpansionModule,
    MatFormFieldModule,
    MatIconModule,
    MatInputModule,
    MatPaginatorModule,
    MatProgressBarModule,
    MatSelectModule,
    MatSlideToggleModule,
    MatSortModule,
    MatToolbarModule,
    MatSnackBarModule, MatProgressSpinnerModule, MatTooltipModule
} from '@angular/material';
import {AddUserComponent} from './actions/add-user/add-user.component';

import {DataTableComponent, ShowTagsDialogComponent, DataTableDialogComponent} from './data-table/data-table.component';
import {ShowTagsComponent} from './actions/show-tags/show-tags.component';
import {AdapterSelectCombobox} from './data-table/combobox/combobox.component';
import {EditConfigComponent, EditAdapterSettingsDialogComponent} from './actions/edit-config/edit-config.component';

const config: SocketIoConfig = {url: 'http://localhost:7777', options: {}};


@NgModule({
    declarations: [
        WBidderComponent,
        AddUserComponent,
        ShowTagsDialogComponent,
        DataTableComponent,
        DataTableDialogComponent,
        ShowTagsComponent,
        AdapterSelectCombobox,
        EditConfigComponent,
        EditAdapterSettingsDialogComponent
    ],
    imports: [
        HttpClientModule,
        BrowserModule,
        AppRoutingModule,
        BrowserAnimationsModule,
        MatFormFieldModule,
        MatInputModule,
        MatPaginatorModule,
        MatSortModule,
        MatButtonModule,
        MatIconModule,
        MatDialogModule,
        MatChipsModule,
        FontAwesomeModule,
        ReactiveFormsModule,
        MatSelectModule,
        MatAutocompleteModule,
        MatCardModule,
        MatCheckboxModule,
        MatSlideToggleModule,
        FormsModule,
        MatDividerModule,
        MatToolbarModule,
        MatProgressBarModule,
        NgxDatatableModule,
        MatExpansionModule,
        MatSnackBarModule,
        MatProgressSpinnerModule,
        SocketIoModule.forRoot(config),
        MatTooltipModule
    ],
    providers: [],
    bootstrap: [WBidderComponent],
    entryComponents: [
        AddUserComponent,
        DataTableDialogComponent,
        ShowTagsDialogComponent,
        EditAdapterSettingsDialogComponent
    ]
})
export class WBidderModule {
}

