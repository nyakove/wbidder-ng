export class User {
    name: string;
    id: number;
    configs: number;
}

export class Config {
    name: string;
    id: number;
    adapters: Array<any>;
    sizes: string;
    type: string;
    cmp: boolean;
    amazon: boolean;
}
