import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {HttpClient, HttpParams} from '@angular/common/http';
import {environment} from './env/env';
import {User} from './models';
import {map} from 'rxjs/operators';

@Injectable(
    {
        providedIn: 'root'
    }
)
export class CrudService {

    constructor(
        private http: HttpClient
    ) {
    }

    getAllAdapters(): Observable<any> {
        return this.http.get(`${environment.url}getAllAdapters`).pipe(map((data: any) => {
            return Object.keys(data);
        }));
    }

    getSettings(adapters: string[]) {
        const DATA = {adaptersList: adapters};
        const body = new HttpParams({fromObject: DATA});
        return this.http.post(`${environment.url}sendConfigs`, body).pipe(map((data: any) => {
            return data;
        }));
    }

    addUserAndConfig(req: string) {
        const DATA = JSON.parse(req);
        delete DATA.toAll;
        delete DATA.userId;
        const body = new HttpParams({fromObject: DATA});
        return this.http.post(`${environment.url}postbidSettings`, body).pipe(map((data: any) => {
            return data;
        }));
    }

    addConfigToUser(req: string) {
        const DATA = JSON.parse(req);
        delete DATA.toAll;
        console.log(DATA);
        const body = new HttpParams({fromObject: DATA});
        return this.http.post(`${environment.url}addConfigToUser`, body).pipe(map((data: any) => {
            return data;
        }));
    }

    editConfig(req: string) {
        const DATA = JSON.parse(req);
        const body = new HttpParams({fromObject: DATA});
        return this.http.post(`${environment.url}editPostbidSettings`, body).pipe(map((data: any) => {
            return data;
        }));
    }

    getUsers(): Observable<User[]> {
        return this.http.get(`${environment.url}getList`).pipe(map((data: any) => {
            const usersList = data.users;
            return usersList.map((user: any) => {
                return {
                    name: user.userData.name,
                    id: user.userData.id,
                    configs: user.configsLength
                };
            });
        }));
    }

    getUserConfigs(id: any): Observable<any> {
        return this.http.get(`${environment.url}getuser/${id}`).pipe(map((data: any) => {
            const configsList = data.configs;
            const configs = configsList.map((config: any) => {
                return {
                    name: config.configname,
                    id: config.configid,
                    adapters: config.adapters,
                    sizes: config.sizes,
                    type: config.type,
                    cmp: config.cmp,
                    amazon: config.amazon
                };
            });
            return {name: data.name, configs};
        }));
    }

    async editUsername(id, name) {
        const params = new HttpParams().set('id', id).set('name', name);
        return new Promise((resolve, reject) => {
            this.http.get(`${environment.url}editPublisherName`, {params, responseType: 'json'})
                .subscribe(data => {
                    resolve(data);
                }, (error => {
                    reject(error);
                }));
        });
    }

    async deleteConfig(id) {
        const params = new HttpParams().set('id', id);
        return new Promise((resolve, reject) => {
            this.http.get(`${environment.url}deleteConfig`, {params, responseType: 'text'})
                .subscribe(data => {
                    resolve(data);
                }, (error => {
                    reject(error);
                }));
        });
    }

    async deleteUsername(id): Promise<string> {
        const params = new HttpParams().set('id', id);
        return new Promise((resolve, reject) => {
            this.http.get(`${environment.url}deleteUser`, {params, responseType: 'text'})
                .subscribe((data: string) => {
                    resolve(data);
                }, (error => {
                    reject(error);
                }));
        });
    }

    async getTags(id) {
        return new Promise((resolve, reject) => {
            this.http.get(`${environment.url}getTags/${id}`)
                .subscribe(data => {
                    resolve(data);
                }, (error => {
                    reject(error);
                }));
        });
    }

    getConfigById(id: any): Observable<any> {
        return this.http.get(`${environment.url}getconfig/${id}`).pipe(map((data: any) => {
            return {
                userId: data.UserId,
                configId: data.id,
                configname: data.config.configname,
                domain: data.config.domain,
                width: data.config.size.width,
                height: data.config.size.height,
                sizes: data.sizes,
                settings: data.config.settings,
                adapters: Object.keys(JSON.parse(data.config.settings)),
                amazon: data.config.amazon,
                amazonID: data.config.amazonID,
                amazonAdUnitCode: data.config.amazonAdUnitCode,
                cmp: data.config.cmp,
                cmpTimeout: data.config.cmpTimeout,
                allowAuctionWithoutConsent: data.config.allowAuctionWithoutConsent,
                PREBID_TIMEOUT: data.config.PREBID_TIMEOUT,
                floorPrice: data.config.floorPrice,
                passbacktag: data.config.passbacktag,
                cdnpath: data.config.cdnpath,
                crid: data.inventory.cr[0].id
            };
        }));
    }


}
