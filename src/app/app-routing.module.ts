import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {AddUserComponent} from './actions/add-user/add-user.component';
import {ShowTagsComponent} from './actions/show-tags/show-tags.component';
import {EditConfigComponent} from './actions/edit-config/edit-config.component';
import {DataTableComponent} from './data-table/data-table.component';

const routes: Routes = [
    {path: '', redirectTo: 'users', pathMatch: 'prefix'},
    {path: 'configs/new', component: AddUserComponent, pathMatch: 'full', data: {title: 'New Config'}},
    {path: 'configs', component: DataTableComponent, data: {title: 'Configs'}},
    {path: 'users/new', component: AddUserComponent, data: {title: 'New User'}},
    {path: 'users', component: DataTableComponent, data: {title: 'Users'}},
    {path: 'configs/edit', component: EditConfigComponent, data: {title: 'Edit Config'}},
    {path: 'tags', component: ShowTagsComponent, data: {title: 'Tags'}},
    {path: '**', redirectTo: 'users'}
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule {
}
