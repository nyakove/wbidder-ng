import {Component, Injectable, OnInit} from '@angular/core';
import {Socket} from 'ngx-socket-io';
/*import {Title} from '@angular/platform-browser';
import {ActivatedRoute} from '@angular/router';*/

@Injectable()

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.scss']
})

export class WBidderComponent implements OnInit {

    constructor(
        private socket: Socket
    ) {
    }

    msg = '...';
    last: any = false;

    ngOnInit() {
        // this.titleService.setTitle('WBidder | ' + this.activatedRoute.snapshot.data.title);
        this.socket.fromEvent('console').subscribe((data: any) => {
            console.log(data);
            if (this.msg === '...') {
                this.msg = '';
            }
            this.msg += `\n${data.mes}`;
            if (data.last === true) {
                setTimeout(function() {
                    this.msg = '...';
                }.bind(this), 5000);
            }
        });
    }
}
