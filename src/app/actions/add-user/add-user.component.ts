import {Component, OnInit, ElementRef, ViewChild, EventEmitter, Output} from '@angular/core';
import {FormBuilder, Validators} from '@angular/forms';
import {MatAutocomplete} from '@angular/material/autocomplete';
import {Location} from '@angular/common';
import {ActivatedRoute, Router} from '@angular/router';
import {Title} from '@angular/platform-browser';

@Component({
    selector: 'app-add-user',
    templateUrl: './add-user.component.html',
    styleUrls: ['./add-user.component.scss']
})
export class AddUserComponent implements OnInit {

    currentRoute: string;
    toAllState: any;
    adapters: string[] = [];
    form = this.fb.group({
        name: ['', [Validators.required, Validators.maxLength(35)]],
        configname: ['', [Validators.required, Validators.maxLength(35)]],
        domain: ['', [Validators.required, Validators.maxLength(35),
            Validators.pattern(/^(https?:\/\/)?([\w.]+)\.([a-z]{2,6}\.?)(\/[\w.]*)*\/?$/)]
        ],
        width: [''],
        height: [''],
        sizes: ['', Validators.required],
        adaptersList: [''],
        amazon: [false],
        cmp: [false],
        allowAuctionWithoutConsent: [false],
        cmpTimeout: [''],
        floorPrice: ['', Validators.required],
        PREBID_TIMEOUT: [1500, Validators.required],
        toAll: [''],
        passbacktag: ['', Validators.required],
        settings: [''],
        userId: ['']
    });
    adaptersList = this.form.controls.adaptersList;
    settings: any;
    allowedSizes: string[] = ['1x1', '88x31', '120x20', '120x30', '120x60', '120x90', '120x240', '120x600', '125x125',
        '160x600', '168x28', '168x42', '180x150', '200x200', '200x446', '216x36', '216x54', '220x90', '234x60',
        '240x133', '240x400', '250x250', '250x350', '250x400', '292x30', '300x31', '300x50', '300x75', '300x100', '300x250',
        '300x600', '320x50', '320x480', '336x280', '468x60', '480x320', '728x90', '768x1024', '970x90', '1024x768',
    ];

    @ViewChild('adaptersInput', {static: false}) adaptersInput: ElementRef<HTMLInputElement>;
    @ViewChild('auto', {static: false}) matAutocomplete: MatAutocomplete;

    @Output() toAllToggle = new EventEmitter();

    constructor(
        private fb: FormBuilder,
        private location: Location,
        private router: Router,
        private activatedRoute: ActivatedRoute,
        private titleService: Title
    ) {
    }

    ngOnInit() {
        this.titleService.setTitle('WBidder | ' + this.activatedRoute.snapshot.data.title);
        this.currentRoute = this.getCurrentRoute();
        if (this.currentRoute.includes('configs')) {
            this.form.controls.name.disable();
            this.form.controls.name.setValue(localStorage.getItem('currentUser'));
        }
    }

    getCurrentRoute() {
        return this.router.routerState.snapshot.url.replace('/', '');
    }

    assignAdaptersSettings() {
        if (!document.getElementById('adapter-settings')) {
            return;
        }
        const settings = {};
        const element: HTMLElement = document.getElementById('adapter-settings');
        const setOfSettings: any = element.getElementsByClassName('adapter-settings-block');
        for (const set of setOfSettings) {
            const adapter = set.getElementsByTagName('mat-panel-title');
            const adapterID = adapter[0].id;
            settings[adapterID] = {};
            const obj = {};
            const thisSettings: any = set.getElementsByClassName('adapter-settings-main');
            for (const s of thisSettings) {
                const option = s.getElementsByTagName('input')[0].placeholder;
                const value = s.getElementsByTagName('input')[0].value;
                const type = s.querySelector('.second-row > span').textContent;
                obj[option] = {data: value, type};
                Object.assign(settings[adapterID], obj);
            }
        }
        return JSON.stringify(settings);
    }

    toggleToAll(checked, value) {
        this.toAllState = {checked, value};
    }

    addUser() {
        this.form.get('adaptersList').setValue(JSON.stringify(this.adapters));
        const settings = this.assignAdaptersSettings();
        const sizes = this.form.get('sizes').value.split('x');
        this.form.get('width').setValue(sizes[0]);
        this.form.get('height').setValue(sizes[1]);
        this.form.get('settings').setValue(settings);
        if (!this.form.get('cmpTimeout').value) {
            this.form.get('cmpTimeout').setValue('undefined');
        }
        localStorage.setItem('query', JSON.stringify(this.form.value));
        localStorage.setItem('route', 'postbidSettings');
        this.router.navigate(['tags'])
            .catch(e => console.log(e));
    }

    addConfig() {
        localStorage.removeItem('query'); // remove old queries if exist
        this.form.get('adaptersList').setValue(JSON.stringify(this.adapters));
        const settings = this.assignAdaptersSettings();
        const sizes = this.form.get('sizes').value.split('x');
        this.form.get('width').setValue(sizes[0]);
        this.form.get('height').setValue(sizes[1]);
        this.form.get('settings').setValue(settings);
        this.form.get('userId').setValue(localStorage.getItem('currentUserId'));
        if (!this.form.get('cmpTimeout').value) {
            this.form.get('cmpTimeout').setValue('undefined');
        }
        this.form.controls.name.enable();
        localStorage.setItem('query', JSON.stringify(this.form.value));
        localStorage.setItem('route', 'addConfigToUser');
        this.form.controls.name.disable();
        this.router.navigate(['tags'])
            .catch(e => console.log(e));
    }

    goBack() {
        this.location.back();
    }

    changeAdaptersList(adapters) {
        this.adapters = adapters;
    }
}
