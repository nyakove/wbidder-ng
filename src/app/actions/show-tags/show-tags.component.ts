import {Component, OnInit} from '@angular/core';
import {Location} from '@angular/common';
import {ActivatedRoute, Router} from '@angular/router';
import {CrudService} from '../../services/crud.service';
import {Title} from '@angular/platform-browser';

@Component({
    selector: 'app-show-tags',
    templateUrl: './show-tags.component.html',
    styleUrls: ['./show-tags.component.scss']
})
export class ShowTagsComponent implements OnInit {

    route: string;
    tags: any;
    fulltag: any;
    passbacktag: any;
    ready = false;
    success = '';
    error = '';
    id = '';

    constructor(
        private location: Location,
        private router: Router,
        private crudService: CrudService,
        private activatedRoute: ActivatedRoute,
        private titleService: Title
    ) {
    }

    ngOnInit() {
        this.titleService.setTitle('WBidder | ' + this.activatedRoute.snapshot.data.title);
        this.route = localStorage.getItem('route');
        this.sendRequest(this.route);
    }

    sendRequest(route) {
        const req = localStorage.getItem('query');
        try {
            if (route === 'postbidSettings') {
                this.crudService.addUserAndConfig(req).subscribe(data => {
                    this.ready = true;
                    this.tags = data.tags;
                    this.fulltag = this.tags.fulltag;
                    this.passbacktag = this.tags.passbackTag;
                    this.success = 'Publisher and config successfully created and saved to database.';
                });
            } else if (route === 'addConfigToUser') {
                this.crudService.addConfigToUser(req).subscribe(data => {
                    this.ready = true;
                    this.tags = data.tags;
                    this.fulltag = this.tags.fulltag;
                    this.passbacktag = this.tags.passbackTag;
                    this.success = 'Config successfully created and saved to database.';
                });
            } else {
                this.crudService.editConfig(req).subscribe(data => {
                    this.ready = true;
                    this.success = 'Config successfully updated.';
                    this.router.navigate(['configs'], {queryParams: {id: data.UserId}})
                        .catch(e => console.log(e));
                });
            }
        } catch (e) {
            this.ready = true;
            this.error = e.message;
            console.log(e);
        }
    }

    back() {
        const id = JSON.parse(localStorage.getItem('query')).UserId
            || JSON.parse(localStorage.getItem('query')).userId;
        if (this.route === 'postbidSettings') {
            this.router.navigate(['users'])
                .catch(e => console.log(e));
        } else {
            this.router.navigate(['configs'], {queryParams: {id}})
                .catch(e => console.log(e));
        }
    }

}
