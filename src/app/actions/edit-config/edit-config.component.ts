import {Component, OnInit, Inject} from '@angular/core';
import {FormBuilder, Validators, FormGroup, FormControl} from '@angular/forms';
import {Location} from '@angular/common';
import {Router, ActivatedRoute} from '@angular/router';
import {CrudService} from '../../services/crud.service';
import {MatDialog, MAT_DIALOG_DATA} from '@angular/material';
import {Title} from '@angular/platform-browser';


export interface DialogData {
    adapter: { name: string, settings: Array<any> };
}

export class EditQuery {
    width: number;
    height: number;
    configname: string;
    domain: string;
    PREBID_TIMEOUT: string;
    floorPrice: number;
    crid: string;
    passbacktag: string;
    cdnpath: string;
    UserId: number;
    configid: number;
    adaptersList: any;
    settings: string;
    amazon: boolean;
    amazonID?: string;
    amazonAdUnitCode?: string;
    cmp: boolean;
    cmpTimeout?: any;
    allowAuctionWithoutConsent?: boolean;
}

@Component({
    selector: 'app-edit-config',
    templateUrl: './edit-config.component.html',
    styleUrls: ['./edit-config.component.scss']
})
export class EditConfigComponent implements OnInit {
    toAllState: any;
    id: number;
    userId: number;
    configName: string;
    existDomain: string;
    existSize: string;
    existAdapters: string[];
    newAdapters: string[];
    existSettings: any;
    newSettings: any;
    amazon: any;
    amazonID: any;
    amazonAdUnitCode: any;
    cmp: any;
    cmpTimeout: any;
    allowAuctionWithoutConsent: boolean;
    crid: string;
    passbacktag: string;
    floorPrice: number;
    PREBID_TIMEOUT: string;
    cdnpath: string;
    ready = false;
    query: EditQuery = new EditQuery();

    allowedSizes: string[] = ['1x1', '88x31', '120x20', '120x30', '120x60', '120x90', '120x240', '120x600', '125x125',
        '160x600', '168x28', '168x42', '180x150', '200x200', '200x446', '216x36', '216x54', '220x90', '234x60',
        '240x133', '240x400', '250x250', '250x350', '250x400', '292x30', '300x31', '300x50', '300x75', '300x100', '300x250',
        '300x600', '320x50', '320x480', '336x280', '468x60', '480x320', '728x90', '768x1024', '970x90', '1024x768',
    ];

    Form = new FormGroup({
        name: new FormControl({value: '', disabled: true}),
        configname: new FormControl({value: '', disabled: true}),
        domain: new FormControl('',
            [Validators.required, Validators.maxLength(35),
                Validators.pattern(/^(https?:\/\/)?([\w.]+)\.([a-z]{2,6}\.?)(\/[\w.]*)*\/?$/)]),
        sizes: new FormControl(''),
        amazon: new FormControl(''),
        cmp: new FormControl(''),
        PREBID_TIMEOUT: new FormControl('', Validators.required),
        floorPrice: new FormControl('', Validators.required),
        passbacktag: new FormControl('', Validators.required),
        toAll: new FormControl(''),
        allowAuctionWithoutConsent: new FormControl(''),
        cmpTimeout: new FormControl(''),
        userId: new FormControl(''),
        adaptersList: new FormControl('')
    });

    constructor(
        private fb: FormBuilder,
        private location: Location,
        private crudService: CrudService,
        private activatedRoute: ActivatedRoute,
        public dialog: MatDialog,
        public router: Router,
        private titleService: Title
    ) {
    }

    ngOnInit() {
        this.titleService.setTitle('WBidder | ' + this.activatedRoute.snapshot.data.title);
        this.activatedRoute.queryParams.subscribe(data => {
            this.id = data.id;
        });
        this.getCurrentConfig(this.id);
    }

    getCurrentConfig(id) {
        this.crudService.getConfigById(id).subscribe(data => {
            console.log(data);
            this.userId = data.userId;
            this.configName = data.configname;
            this.existDomain = data.domain;
            this.existSize = data.sizes;
            this.existAdapters = data.adapters;
            this.existSettings = JSON.parse(data.settings);
            this.amazon = data.amazon === 'true';
            this.amazonID = data.amazonID;
            this.amazonAdUnitCode = data.amazonAdUnitCode;
            this.cmp = data.cmp === 'true';
            this.cmpTimeout = data.cmpTimeout;
            this.allowAuctionWithoutConsent = data.allowAuctionWithoutConsent === 'true';
            this.crid = data.crid;
            this.passbacktag = data.passbacktag;
            this.floorPrice = parseFloat(data.floorPrice);
            this.PREBID_TIMEOUT = data.PREBID_TIMEOUT;
            this.cdnpath = data.cdnpath;
            this.ready = true;

            this.Form.patchValue({
                name: localStorage.currentUser || undefined,
                configname: this.configName,
                domain: this.existDomain,
                sizes: this.existSize,
                amazon: this.amazon,
                cmp: this.cmp,
                cmpTimeout: this.cmpTimeout,
                allowAuctionWithoutConsent: this.allowAuctionWithoutConsent,
                PREBID_TIMEOUT: this.PREBID_TIMEOUT,
                floorPrice: this.floorPrice,
                passbacktag: this.passbacktag
            });
        });
    }

    changeAdaptersList(adapters) {
        this.newAdapters = adapters;
    }

    goBack() {
        this.location.back();
    }

    editAdapterSettings(adapter) {
        const settingsArray = Object.entries(this.existSettings[adapter]);
        const set = {};
        set[adapter] = {};

        const dialogRef = this.dialog.open(EditAdapterSettingsDialogComponent,
            {data: {adapter: {name: adapter, settings: settingsArray}}});
        dialogRef.afterClosed().subscribe((form: any) => {
            if (form) {
                const inputs = form.getElementsByTagName('input');
                for (const f of inputs) {
                    let key = f.placeholder;
                    let data = f.value;
                    let type = f.title;
                    Object.assign(set[adapter], {[key]: {data, type}});
                }
                this.existSettings[adapter] = set[adapter];
            }
        });
    }

    toggleToAll(checked, value) {
        this.toAllState = {checked, value};
    }

    assignAdaptersSettings() {
        if (!document.getElementById('adapter-settings')) {
            return null;
        }
        const settings = {};
        const element: HTMLElement = document.getElementById('adapter-settings');
        const setOfSettings: any = element.getElementsByClassName('adapter-settings-block');
        for (const set of setOfSettings) {
            const adapter = set.getElementsByTagName('mat-panel-title');
            const adapterID = adapter[0].id;
            settings[adapterID] = {};
            const obj = {};
            const thisSettings: any = set.getElementsByClassName('adapter-settings-main');
            for (const s of thisSettings) {
                const option = s.getElementsByTagName('input')[0].placeholder;
                const value = s.getElementsByTagName('input')[0].value;
                const type = s.querySelector('.second-row > span').textContent;
                obj[option] = {data: value, type};
                Object.assign(settings[adapterID], obj);
            }
        }
        return settings;
    }

    editConfig() {
        const sizes = this.Form.controls.sizes.value.split('x');
        this.query.width = parseInt(sizes[0], 10);
        this.query.height = parseInt(sizes[1], 10);
        this.query.configname = this.configName;
        this.query.domain = this.Form.controls.domain.value;
        this.query.PREBID_TIMEOUT = this.Form.controls.PREBID_TIMEOUT.value;
        this.query.floorPrice = this.Form.controls.floorPrice.value;
        this.query.crid = this.crid;
        this.query.passbacktag = this.Form.controls.passbacktag.value;
        this.query.cdnpath = this.cdnpath;
        this.query.UserId = this.userId;
        this.query.configid = this.id;
        this.query.amazon = this.Form.controls.amazon.value;
        this.query.amazonID = this.amazonID;
        this.query.amazonAdUnitCode = this.amazonAdUnitCode;
        this.query.cmp = this.Form.controls.cmp.value;
        this.query.cmpTimeout = this.Form.controls.cmpTimeout.value || undefined;
        this.query.allowAuctionWithoutConsent = this.Form.controls.allowAuctionWithoutConsent.value;
        this.query.adaptersList = this.newAdapters
            ? JSON.stringify(this.existAdapters.concat(this.newAdapters))
            : JSON.stringify(this.existAdapters);
        this.newSettings = this.assignAdaptersSettings();
        this.query.settings = !this.newSettings
            ? JSON.stringify(this.existSettings)
            : JSON.stringify(Object.assign(this.existSettings, this.newSettings));
        localStorage.setItem('query', JSON.stringify(this.query));
        localStorage.setItem('route', 'editPostbidSettings');
        this.router.navigate(['tags'])
            .catch(e => console.log(e));
    }

}


@Component({
    selector: 'app-edit-adapter-settings-dialog-component',
    templateUrl: './edit-adapter-settings-dialog-component.html',
    styleUrls: ['./edit-adapter-settings-dialog-component.scss']
})
export class EditAdapterSettingsDialogComponent {
    constructor(@Inject(MAT_DIALOG_DATA)
                public data: DialogData) {
    }
}
